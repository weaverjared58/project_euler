/**
 * 
 */
package pe_main;


import java.util.ArrayList;

/**
 * @author Poseidon
 *
 */
public class DigitEnglish {
	
	private int integerValue;
	private String englishValue;
//	private String englishNumberValue
	
	private ArrayList<String> englishDigits = new ArrayList<String>();
	{
		this.englishDigits.add("zero");
		this.englishDigits.add("one");
		this.englishDigits.add("two");
		this.englishDigits.add("three");
		this.englishDigits.add("four");
		this.englishDigits.add("five");
		this.englishDigits.add("six");
		this.englishDigits.add("seven");
		this.englishDigits.add("eight");
		this.englishDigits.add("nine");
	}

	/**
	 * 
	 */
	public DigitEnglish() {
		// TODO Auto-generated constructor stub
		this.integerValue = 0;
		this.englishValue = this.englishDigits.get(this.integerValue);
	}
	
	/**
	 * 
	 */
	public DigitEnglish(int a) {
		// TODO Auto-generated constructor stub
		this.integerValue = a;
		this.englishValue = this.englishDigits.get(this.integerValue);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		DigitEnglish A = new DigitEnglish(4);
		System.out.println(A.getEnglishValue());
		

	}

	private String getEnglishValue() {
		return this.englishValue;
	}

}

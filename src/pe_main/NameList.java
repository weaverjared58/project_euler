package pe_main;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class NameList {

	private ArrayList<Name> storage = new ArrayList<Name>();

	public NameList(Name arg) {
		// TODO Auto-generated constructor stub
//		System.out.println("Here.");
		this.storage.add(arg);
	}

	public NameList() {
		// TODO Auto-generated constructor stub
	}

	public void add(Name arg) {
		this.storage.add(arg);
	}

	public void print() {
		for (int i = 0; i < this.storage.size(); i++) {
			System.out.println(this.storage.get(i).strForm);
		}

	}

	public void sort() {
		// TODO Auto-generated method stub
		// Sorting the strings
		for (int i = 0; i < this.storage.size(); i++) {
			for (int j = i + 1; j < this.storage.size(); j++) {
				Name namei = this.storage.get(i);
				Name namej = this.storage.get(j);
				String stri = namei.strForm;
				String strj = namej.strForm;
				if (stri.compareTo(strj) > 0) {
					this.storage.set(i, namej);
					this.storage.set(j, namei);
				}
			}
		}

	}

	public int getSize() {
		// TODO Auto-generated method stub
		return this.storage.size();
	}

	public Name getElement(int i) {
		// TODO Auto-generated method stub
		return this.storage.get(i);
	}

	public void importNamefile(String pathToCsv) {
		// TODO Auto-generated method stub
		System.out.println("Importing file <"+pathToCsv+">.");
		try {
			BufferedReader csvReader = new BufferedReader(new FileReader(pathToCsv));
			String row;
			while ((row = csvReader.readLine()) != null) {
				String[] data = row.split(",");
				for (int i = 0; i < data.length; i++) {
					// System.out.println(data[i]);
					this.storage.add(new Name(data[i]));
				}

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Import complete.");

	}

	public void printListScore() {
		// TODO Auto-generated method stub
		int nameListScore = 0;
		for (int i = 0; i < this.getSize(); i++) {
			nameListScore += (i + 1) * this.getElement(i).numSum;
			//System.out.println(i + 1 + "," + this.getElement(i).strForm + "," + this.getElement(i).numSum);
		}
		System.out.println("nameListScore=" + nameListScore);

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		ArrayList<String> names = new ArrayList<String>();
//		names.add("Rick");
//		names.add("Steve");
//		names.add("Abey");
//		names.add("Lina");
//		names.add("Chaitanya");

		Name nameA = new Name("Rick");
		Name nameB = new Name("Steve");
		Name nameC = new Name("Abey");
		NameList names = new NameList(nameA);
		names.add(nameB);
		names.add(nameC);
		// names.print();
		assertEquals("Rick", names.getElement(0).strForm);
		assertEquals(41, names.getElement(0).numSum);
		names.sort();
		// names.print();
		names.printListScore();
		assertEquals("Abey", names.getElement(0).strForm);
		assertEquals(33, names.getElement(0).numSum);
		
		

		Date date = new Date(); // This object contains the current date value
		SimpleDateFormat formatter = new SimpleDateFormat("yyyymmddHHmmss");
		String timestamp = formatter.format(date);
		String dbname = "Names_" + timestamp + ".db";
		names.writeDb(dbname);

	}

	public void writeDb(String dbname) {
		// TODO Auto-generated method stub		
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");

			String url = "jdbc:sqlite:./dat/" + dbname;
			conn = DriverManager.getConnection(url);
			conn.setAutoCommit(false);
			System.out.println("Connection to SQLite has been established.");

			// Perform actions on the active database.
			stmt = conn.createStatement();
			String sql = "CREATE TABLE NAMES ("
					+ " ID             INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
					+ " NAME           TEXT  NOT NULL, " 
					+ " LENGTH         INT   NOT NULL, " 
					+ " CHARSCORE      INT   NOT NULL, "
					+ " ALPHARANK      INT   NOT NULL, "
					+ " NAMESCORE      INT   NOT NULL"
					+")";
			stmt.executeUpdate(sql);

//			sql = "INSERT INTO NAMES (NAME,LENGTH,CHARSCORE,ALPHARANK) "
//					+ " VALUES ('Paul', 32, 11, 1 );";
//			stmt.executeUpdate(sql);
			for (int i = 0; i < this.storage.size(); i++) {
				Name temp = this.getElement(i);
				int rank = i+1;
				int namescore = rank*temp.numSum;
				sql = "INSERT INTO NAMES (NAME,LENGTH,CHARSCORE,ALPHARANK,NAMESCORE) "
						+ " VALUES ('"+temp.strForm+"', "
						+ temp.strForm.length() +", "
						+ temp.numSum +", "
						+ rank + ", "
						+ namescore 
						+")";
				stmt.executeUpdate(sql);
			}

//			sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " + " VALUES (2, 'Allen', 25, 'Texas', 15000.00 );";
//			stmt.executeUpdate(sql);
//
//			sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " + " VALUES (3, 'Teddy', 23, 'Norway', 2000.00 );";
//			stmt.executeUpdate(sql);
//
//			sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
//					+ " VALUES (4, 'Mark', 25, 'Rich-Mond', 65000.00 );";
//			stmt.executeUpdate(sql);

			stmt.close();
			conn.commit();
			conn.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			// System.err.println(e.getMessage());
			System.exit(0);
		} finally {
			// System.out.println("executing 'finally' code block...");
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
		}
		System.out.println("Table and records created and successfully on " + dbname + ".");
		
	}

}

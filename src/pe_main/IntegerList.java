package pe_main;

import java.util.ArrayList;
import java.util.Collections;

public class IntegerList {
	private ArrayList<Integer> storage;
	private int storageSum;
	private boolean sumValid = false;

	public IntegerList(int upperbound) {
		// TODO Auto-generated constructor stub
		System.out.println("Note: IntegerList class deprecated 20200728 to give way to the more useful PrimesList class.");
		
		System.out.println("--- Constructing instance of IntegerList object ---");
		this.storage = new ArrayList<Integer>();
		for (int i = 0; i < upperbound; i++) {
			storage.add(i);
		}
		//this.sumStorage();
	}

	public int sumStorage() {
		System.out.println("--- Summing storage ---");
		int sum = 0;
		for (int i = 0; i < this.storage.size(); i++) {
			sum += this.storage.get(i);
		}
		this.storageSum = sum;
		this.sumValid = true;
		return storageSum;
	}
	
	public int SumOfSquares() {
		int sum = 0;
		for (int i = 0; i < this.storage.size(); i++) {
			sum += Math.pow(this.storage.get(i),2);
		}
		return sum;
	}
	
	public int SquareOfSum() {
		int sum = 0;
		for (int i = 0; i < this.storage.size(); i++) {
			sum += this.storage.get(i);
		}
		return (int) Math.pow(sum, 2);
	}

	public void printStorage() {
		System.out.println("IntegerList contents:");
		for (int i = 0; i < storage.size(); i++) {
			System.out.println(storage.get(i));
		}
	}

	public void printSum() {
		if (sumValid) {
			System.out.println("Sum of elements in storage is: " + storageSum);
		} else {
			System.out.println("Sum of elements is not currently up to date. Please recompute sum.");
		}
	}

//	@SuppressWarnings("null")
	public void sieveComposites() {
		System.out.println("--- Sieving composites ---");
		// This is an implementation of the Sieve of Eratosthenes,
		// to reduce the list's elements in storage to only prime numbers.

		this.sumValid = false;
		ArrayList<Integer> indicesToRemove = new ArrayList<Integer>();
		// purge the unique exceptions 0 and 1
//		for (int i = 0; i < storage.size(); i++) {
//			if (storage.get(i) == 0) {
//				indicesToRemove.add(i);
//			} else if (storage.get(i) == 1) {
//				indicesToRemove.add(i);
//			}
//		}
		
		indicesToRemove.add(0);
		indicesToRemove.add(1);
		
		
		// implement sieve to remove any multiples of primes up to the upperbound
		// start with:
		// 2
		// 3
		// 5
		// 7
		// 11
		// ... basically, work your way up the list sequentially and execute sieve using current element
		//     iff the index of the current element is not already a member of the indicesToRemove ArrayList.
		for (int i = 0; i < storage.size(); i++) {
			if (!indicesToRemove.contains(i)) {
				int d = storage.get(i);
				for (int j=i+d; j<storage.size(); j+=d) {
					if (!indicesToRemove.contains(j)) {
						indicesToRemove.add(j);
						System.out.println(j);
					}
				}
			}
		}
		
		
		purgeElements(indicesToRemove);

	}
	
//	public void sieve2() {
//		System.out.println("Sieving 2");
//		storage.set(0,0);
//		storage.set(1,0);
//		for
//		for (int i = 2; i<storage.size(); i+=d)
//	}

	private void purgeElements(ArrayList<Integer> indices) {
		System.out.println("--- Purging composites ---");
		ArrayList<Integer> temp = new ArrayList<Integer>();
		//temp = this.storage;
		//Collections.copy(temp, storage);
		
		temp = (ArrayList<Integer>)this.storage.clone();
		
		System.out.println(temp.size()+" "+storage.size());
		storage.clear();
		System.out.println(temp.size()+" "+storage.size());
		// for (int i = 0; i < indices.length; i++) {
		for (int i = 0; i < temp.size(); i++) {
			if (!indices.contains(i))
				storage.add(i);
		}
		System.out.println(indices.size() + " elements removed from storage!");
		System.out.println("IntegerList storage now has " + storage.size() + " elements.");

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int max = (int)2e2;
		IntegerList a = new IntegerList(max);
		//a.printStorage();
		//a.printSum();
//		int sum = a.sumStorage();
//		System.out.println(sum);

		a.sieveComposites();
		//a.printStorage();
		a.printSum();
		a.sumStorage();
		a.printSum();
		System.out.println();

		max = 101;
		IntegerList b = new IntegerList(max);
		//b.printStorage();
		System.out.println(b.SumOfSquares());
		System.out.println(b.SquareOfSum());
		System.out.println(b.SumOfSquares() - b.SquareOfSum());

	}

}

package pe_main;

import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

public class ExtraLongInteger {
	private ArrayList<Integer> storage = new ArrayList<Integer>();

	public ExtraLongInteger(int n) {
		String str_n = String.valueOf(n);
		int length = str_n.length();

		for (int i = length - 1; i > -1; i--) {
			this.storage.add(Character.getNumericValue(str_n.charAt(i)));
		}
	}

	public ExtraLongInteger(String str_n) {
		int length = str_n.length();

		for (int i = length - 1; i > -1; i--) {
			this.storage.add(Character.getNumericValue(str_n.charAt(i)));
		}
	}

	public void printNumber() {
		for (int i = this.storage.size() - 1; i > -1; i--) {
			System.out.print(storage.get(i));
		}
		System.out.println();
	}

	public void multiplyByInt(int m) {
		// Ref: <https://www.geeksforgeeks.org/factorial-large-number/>
		int prod = 0;
		int store;
		int carry = 0;
		String str_prod;
		int initialSize = this.storage.size();
		// for (int i = this.storage.size() - 1; i > -1; i--) {
		for (int i = 0; i < initialSize; i++) {
			// System.out.println(i + ", " + storage.get(i));
			prod = storage.get(i) * m + carry;
			// System.out.println(prod);
			str_prod = String.valueOf(prod);
			store = Character.getNumericValue(str_prod.charAt(str_prod.length() - 1));
			str_prod = str_prod.substring(0, str_prod.length() - 1);
			if (str_prod.length() == 0)
				carry = 0;
			else
				carry = Integer.parseInt(str_prod);

			// System.out.println(prod + ", " + store + ", " + carry);
			storage.set(i, store);
		}
		// System.out.println(carry);
		if (carry != 0) {
			String str_carry = String.valueOf(carry);
			for (int i = str_carry.length() - 1; i > -1; i--) {
				this.storage.add(Character.getNumericValue(str_carry.charAt(i)));
			}			
		}
	}

	public String getNumber() {
		// TODO Auto-generated method stub
		String str_num = "";
		for (int i = this.storage.size() - 1; i > -1; i--) {
			// System.out.print(storage.get(i));
			str_num = str_num + String.valueOf(storage.get(i));
		}
		return str_num;
	}
	
	public int getSize() {
		// TODO Auto-generated method stub
		return storage.size();
	}
	

	/**
	 * @param n
	 */
	public static ExtraLongInteger computeLongFactorial(int n) {
		ExtraLongInteger N = new ExtraLongInteger(n);
		N.printNumber();
		// Compute ExtraLargeNumber factorial value of n
		for (int i = n-1; i>0; i--) {
			N.multiplyByInt(i);
		}
		//N.printNumber();
		//System.out.println(N.getSize());
		//System.out.println(N.getNumber());
		return N;
	}


	//private Integer getElement(int index) {
	public Integer getElement(int index) {
		// TODO Auto-generated method stub
		return this.storage.get(index);
	}
	

	/**
	 * @param n
	 * @return
	 */
	public static int sumDigits(ExtraLongInteger n) {
		int sum = 0;
		for (int i = 0; i<n.getSize(); i++) {
			sum = sum + n.getElement(i);
		}
		int digitSum = sum;
		return digitSum;
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a = 9815;
		ExtraLongInteger A = new ExtraLongInteger(a);

		// A.printNumber();
		System.out.println(A.getNumber());
		System.out.println(A.getSize());

		A.multiplyByInt(11);
		// A.printNumber();
		System.out.println(A.getNumber());
		System.out.println(A.getSize());
		assertEquals("107965", A.getNumber());
		A.printNumber();

		// test dual constructor from String (since this is more practical anyway;) )
		String b = "9815";
		ExtraLongInteger B = new ExtraLongInteger(b);
		assertEquals("9815", B.getNumber());
		
		for (int i = 0; i<B.getSize(); i++) {
			System.out.println(B.getElement(i));
		}

	}



}

/**
 * This project is oriented at building fluency with Java and OOP programming, and in Eclipse IDE, through 
 * constructing solutions to mathematical puzzles under the Project Euler (PE) project.
 * 
 * Reference site: <https://projecteuler.net/>
 */
package pe_main;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
//import org.apache.commons.math3.util.CombinatoricsUtils;
//import org.apache.commons.math3.util.CombinatoricsUtils.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
//import java.lang.*;
//import java.util.concurrent.TimeUnit;
import java.lang.Thread;
import java.text.SimpleDateFormat;

import pe_main.ExtraLongInteger;
import sandbox.RuntimeExec;
//import sandbox.RuntimeExec;

import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.math3.util.MathUtils;

/**
 * @author Poseidon
 *
 */
public class PE_solver {

	/**
	 * 
	 */
	private static void solvePE022() {
		System.out.println("\n--- Solving PE #022 ---");
		NameList names2 = new NameList();
		String namefile = "./dat/p022_names.txt";
		names2.importNamefile(namefile);
		// names2.print();
		System.out.println("size: " + names2.getSize());
		names2.printListScore();
		names2.sort();
		names2.printListScore();
		
		
		// Establish a name for the sqlite3 database file.
		Date date = new Date(); // This object contains the current date value
		SimpleDateFormat formatter = new SimpleDateFormat("yyyymmddHHmmss");
		String timestamp = formatter.format(date);
		String dbname = "Names_" + timestamp + ".db";
		names2.writeDb(dbname);
	}

	private static void solvePE020() {
		System.out.println("\n--- Solving PE #020 ---");
		int n = 100;
		ExtraLongInteger nFactorial = ExtraLongInteger.computeLongFactorial(n);
		System.out.println(nFactorial.getSize());

		int digitSum = ExtraLongInteger.sumDigits(nFactorial);
		System.out.println(digitSum);
		System.out.println();
	}

	private static void solvePE006() {
		System.out.println("\n--- Solve PE #006 ---");
		int max1 = 101;
		IntegerList c = new IntegerList(max1);
		// b.printStorage();
		System.out.println(c.SumOfSquares());
		System.out.println(c.SquareOfSum());
		System.out.println(c.SumOfSquares() - c.SquareOfSum());
	}

	private static void solvePE002() {
		System.out.println("\n--- Solve PE #002 ---");

		int a = 1;
		int b = 2;
		int temp;
		int sumeven = b;
		int max = 4000000;
		int upperIndex = 50;
		for (int i = 3; i < upperIndex; i++) {
			temp = b;
			b += a;
			a = temp;
			if (Math.abs(b) < max) {
				// System.out.println(b);
				// System.out.println(Math.abs(b));
				if (b % 2 == 0)
					sumeven += b;
				if (i == upperIndex - 1)
					System.out.println("--- Caution, may not have reached upper fibonacci term limit. ---");
			} else {
				System.out.println("Exceeded max fibonacci term at index " + i);
				break;
			}
			// check to make sure we got all the way to the specified max value in the
			// fibonacci sequence
		}
		System.out.println("Sum of even terms below four million: " + sumeven);
		System.out.println();
	}

	private static void solvePE001() {
		System.out.println("\n--- Solve PE #001 ---");
		int factor1, factor2, factor3, upperLimit;
		factor1 = 3;
		factor2 = 5;
		factor3 = factor1 * factor2;
		upperLimit = 1000;

		int[] indices1 = new int[1000];
		int[] indices2 = new int[1000];
		int[] indices3 = new int[1000];

		for (int i = 1; i < upperLimit; i++) {
			int prod1 = factor1 * i;
			if (prod1 < 1000) {
				indices1[i] = i;
			}
			int prod2 = factor2 * i;
			if (prod2 < upperLimit) {
				indices2[i] = i;
			}
			int prod3 = factor3 * i;
			if (prod3 < upperLimit) {
				indices3[i] = i;
			}
		}

		// Sum and subtract one instance of like multiples
		int sum1 = 0;
		int sum2 = 0;
		int sum3 = 0;
		int endsum;
		for (int i = 1; i < indices1.length; i++) {
			sum1 += factor1 * indices1[i];
			sum2 += factor2 * indices2[i];
			sum3 += factor3 * indices3[i];
		}
		endsum = sum1 + sum2 - sum3;
		System.out.println("Sum of factor1 and factor2 multiples below 1000: " + endsum);
		System.out.println();
	}

	/**
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) {
		CommandLine commandLine;
		Option option_test = Option.builder().longOpt("test").desc("The test option").build();
		Options options = new Options();
		CommandLineParser parser = new DefaultParser();

		options.addOption(option_test);

		String header = "               [<arg1> [<arg2> [<arg3> ...\n       Options, flags and arguments may be in any order";
//	    String footer = "This is DwB's solution brought to Commons CLI 1.3.1 compliance (deprecated methods replaced)";
		String footer = "(usage footer)";
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("CLIsample", header, options, footer, true);
		System.out.println();

//	    String[] testArgs =
//	            { "-r", "opt1", "-S", "opt2", "arg1", "arg2",
//	                    "arg3", "arg4", "--test", "-A", "opt3", };
//	    String[] testArgs =
//            { "-r", "opt1things",  "arg1", "arg2",
//                    "arg3", "arg4", "-A", "opt3things", };

		try {
			commandLine = parser.parse(options, args);

//			if (commandLine.hasOption("A")) {
//				System.out.print("Option A is present.  The value is: ");
//				System.out.println(commandLine.getOptionValue("A"));
//			}

//			if (commandLine.hasOption("r")) {
//				System.out.print("Option r is present.  The value is: ");
//				System.out.println(commandLine.getOptionValue("r"));
//			}

//			if (commandLine.hasOption("S")) {
//				System.out.print("Option S is present.  The value is: ");
//				System.out.println(commandLine.getOptionValue("S"));
//			}

			if (commandLine.hasOption("test")) {
				System.out.println("Option test is present.  This is a flag option.");
				System.out.println();
				System.out.println(
						"--- '--test' flag passed to command line program, printing extra info about cmd line args String array ---");
				System.out.println("User args are: ");
				if (args.length > 0) {
					char[] caMainArg = null;
					String strMainArg = null;

					for (String arg : args) {
						// Convert each String arg to char array, use case?
						caMainArg = arg.toCharArray();

						// convert char array to String
						strMainArg = new String(caMainArg);

						System.out.println(strMainArg + ", length " + strMainArg.length());

					}
				} else {
					System.out.println("No arguments were passed from command line.");
				}
				System.out.println();
				System.out.println("And the length of the args String array is " + args.length + ".");

			}

			String[] remainder = commandLine.getArgs();
			System.out.print("Remaining arguments (currently unused and not paired with a user option id character): ");
			for (String argument : remainder) {
				System.out.print(argument);
				System.out.print(" ");
			}

			System.out.println();

		} catch (ParseException exception) {
			System.out.print("Parse error: ");
			System.out.println(exception.getMessage());
		}

		// Solve PE001
		solvePE001();

		// Solve PE002
		solvePE002();

		// Solve PE003
		// System.out.println("\n--- Solve PE #003 ---");

		// Solve PE006
		solvePE006();

		// Solve PE010
		System.out.println("\n--- Solving PE #010 ---");
		int max = (int) 2.0e3;

		// IntegerList a = new IntegerList(200);
		// a.sumStorage();
		// a.printSum();
		PrimesList b = new PrimesList(max);
		b.getSize();

		// Solve PE020
		solvePE020();

//		// Solve PE015
//		System.out.println("\n--- Solving PE #015 ---");
//
//		// q: how long to just increment a counter from 1 to 137,846,528,820?
//		double max2 = 1.5*(137e7+1);
//		int sum = 0;
//		long startTime = System.nanoTime();
//		for (int i = 0; i < max2; i++)
//			sum = sum+1;
//		// TimeUnit.SECONDS.sleep(3);
//		long endTime = System.nanoTime();
//		System.out.println(sum);
//		double duration_sec = (endTime - startTime) * Math.pow(10, -9);
//		System.out.println("duration: " + duration_sec + " seconds");

		// Solve PE022
		solvePE022();

		System.out.println();
		// Small examples of querying cpu's thru java and also the Windows cmd line.
//		int cores = Runtime.getRuntime().availableProcessors();
//        System.out.println("Available processor cores is "+cores);
//        System.out.println(Thread.currentThread().getName());
//        System.out.println(Thread.currentThread().getId());
//                		
//		int n_commands = 2;
//        String[] execCmd = new String[n_commands];
//        execCmd[0] = "ping localhost";
//        execCmd[1] = "wmic cpu get deviceid";
//        RuntimeExec.main(execCmd);

	}

}

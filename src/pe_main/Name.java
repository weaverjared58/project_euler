package pe_main;

import java.util.ArrayList;

public class Name {
	final String alphabet = "abcdefghijklmnopqrstuvwxyz";
	
	String strForm;
	ArrayList<Integer> numForm;
	int numSum;

	public Name() {
		// TODO Auto-generated constructor stub
		System.out.println("Default constructor used, ergo default name is input.");
		strForm = "Rick";
        this.numForm = new ArrayList<Integer>();
        String temp = strForm.toLowerCase();
        for(int i=0; i < temp.length(); i++){
        	numForm.add(alphabet.indexOf(temp.charAt(i))+1);
//        	System.out.println(nameNum.get(i));
//            System.out.print(alphabet.indexOf(input.charAt(i))+1 + "\n");
        }
        
        this.numSum = sumNumForm();
//        System.out.println(numSum);
	}
	

	public Name(String arg) {
		// TODO Auto-generated constructor stub
		strForm = arg;
        this.numForm = new ArrayList<Integer>();
        String temp = strForm.toLowerCase();
        for(int i=0; i < temp.length(); i++){
        	numForm.add(alphabet.indexOf(temp.charAt(i))+1);
//        	System.out.println(nameNum.get(i));
//            System.out.print(alphabet.indexOf(input.charAt(i))+1 + "\n");
        }
        
        this.numSum = sumNumForm();
//        System.out.println(numSum);
	}

	private int sumNumForm() {
		// TODO Auto-generated method stub
        int sum = 0;
//        System.out.println(numForm.size());
        for (int i = 0; i < numForm.size(); i++) {
        	sum += numForm.get(i);
        }
//        System.out.println(sum);
		return sum;
	}
	
	public static void main(String[] args) {
		Name nameA = new Name();
		System.out.println("Name string: "+nameA.strForm+", Name sum: "+nameA.numSum);
		

		Name nameB = new Name("Steve");
		System.out.println("Name string: "+nameB.strForm+", Name sum: "+nameB.numSum);
		
		
	}

}

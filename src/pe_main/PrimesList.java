package pe_main;

import java.util.ArrayList;
import java.util.Iterator;

public class PrimesList {
	private ArrayList<Integer> storage;
	private int storageSum;
	private boolean sumValid = false;
	private int lowerbound = 0;
	private int upperbound;

//	public PrimesList(int lowerbound, int upperbound) {
//		// TODO Auto-generated constructor stub
//		System.out.println("\n--- Constructing instance of PrimesList object ---");
//		this.lowerbound = lowerbound;
//		this.upperbound = upperbound;
//		this.storage = new ArrayList<Integer>();
//		for (int i = lowerbound; i < upperbound; i++) {
//			storage.add(i);
//		}
//		this.sieveComposites2();
//	}

	public PrimesList(int upperbound) {
		// TODO Auto-generated constructor stub
		System.out.println("\n--- Constructing instance of PrimesList object ---");
		this.upperbound = upperbound;
		this.storage = new ArrayList<Integer>();
		for (int i = 0; i < upperbound; i++) {
			storage.add(i);
		}
		this.sieveComposites();
	}

	public PrimesList(int upperbound, boolean flag) {
		// TODO Auto-generated constructor stub
		System.out.println("\n--- Constructing instance of PrimesList object, by beta constructor ---");
		this.upperbound = upperbound;
		this.storage = new ArrayList<Integer>();
//		for (int i = 0; i < upperbound; i++) {
//			storage.add(i);
//		}

		storage.add(2);
		for (int i = 3; i < upperbound; i += 2) {
			storage.add(i);
		}
		this.sieveComposites2();
	}

	public int sumStorage() {
		System.out.println("--- Summing storage ---");
		int sum = 0;
		for (int i = 0; i < this.storage.size(); i++) {
			sum += this.storage.get(i);
		}
		this.storageSum = sum;
		this.sumValid = true;
		return storageSum;
	}

	public void printSize() {
		System.out.println("Size of storage array: " + this.storage.size());
		System.out.println();
	}

	public int getSize() {
		// System.out.println("Size of storage array: "+this.storage.size());
		// System.out.println();
		return this.storage.size();
	}

	public int getStorageSum() {
		return this.storageSum;
	}

	public void printStorage() {
		System.out.println("storage contents:");
		if (storage.size() > 1000) {
			System.out.println("Warning: storage contents large, >1000, so print is disabled.");
		} else {
			for (int i = 0; i < storage.size(); i++) {
//				try {
//					System.out.println(storage.get(i)+" "+storage.get(i+1)+" "+storage.get(i+2)+" "+storage.get(i+3)+" "+storage.get(i+4));		
//				} finally {
//					
//				}
				System.out.print(storage.get(i) + " ");
				if ((i + 1) % 10 == 0) {
					System.out.print("\n");
				} else if (i == storage.size() - 1) {
					System.out.print("\n");
				}
			}
			System.out.println();
		}
	}

	public void printSum() {
		if (sumValid) {
			System.out.println("Sum of elements in storage is: " + storageSum);
		} else {
			System.out.println("Sum of elements is not currently up to date. Please recompute sum.");
		}
		System.out.println();
	}

	public void sieveComposites() {
		System.out.println("--- Sieving composites ---");
		// This is an implementation of the Sieve of Eratosthenes,
		// to reduce the list's elements in storage to only prime numbers.

		this.sumValid = false;

		// long startTime;
		// startTime = System.nanoTime();
		ArrayList<Integer> indicesToRemove = computeCompositeIndices();
		// System.out.println("Time to create composites index list: " +
		// (System.nanoTime() - startTime) * Math.pow(10, -9) + " seconds");

		// startTime = System.nanoTime();
		purgeElements(indicesToRemove);
		// System.out.println("Time to remove composites from storage: " +
		// (System.nanoTime() - startTime) * Math.pow(10, -9) + " seconds");

		System.out.println("Purge complete.");
		System.out.println();
	}

	public void sieveComposites2() {
		System.out.println("--- Sieving composites ---");
		// This is an implementation of the Sieve of Eratosthenes,
		// to reduce the list's elements in storage to only prime numbers.

		this.sumValid = false;

//		//long startTime;
//		//startTime = System.nanoTime();
//		ArrayList<Integer> indicesToRemove = computeCompositeIndices();
//		//System.out.println("Time to create composites index list: " + (System.nanoTime() - startTime) * Math.pow(10, -9) + " seconds");
//
//		//startTime = System.nanoTime();
//		purgeElements(indicesToRemove);
//		//System.out.println("Time to remove composites from storage: " + (System.nanoTime() - startTime) * Math.pow(10, -9) + " seconds");
//		
		// purge the unique exceptions 0 and 1
		this.storage.remove(new Integer(0));
		this.storage.remove(new Integer(1));

		// implement sieve to remove any multiples of primes up to the upperbound
		// start with:
		// 2
		// 3
		// 5
		// 7
		// 11
		// ... basically, work your way up the list sequentially and execute sieve using
		// current element
		// iff the index of the current element is not already a member of the
		// indicesToRemove ArrayList.
		// int size = storage.size();
		// for (int i = 0; i < size; i++) {

		// Precondition to remove the first easy composites sets:
		//System.out.println(this.storage.size());
		
		int divisor = 0;
		int remainder = 0;
		for (int i = 0; i < storage.size(); i++) {
			divisor = storage.get(i);
			//System.out.println(divisor);
			for (int j = i + 1; j < storage.size(); j++) {
				remainder = storage.get(j) % divisor;
				if (remainder == 0) 
					storage.remove(j);
			}

		}
		System.out.println(this.storage.size());

//		for (int i = 0; i < storage.size(); i++) {
//			int d = storage.get(i);
//			int composite = d + d;
//			while (composite < this.upperbound) {
//				this.storage.remove(new Integer(composite));
//				composite += d;
//			}
//			// size = storage.size();
//			// System.out.println(this.storage.size());
//		}


		System.out.println("Purge complete.");
		System.out.println();
	}

	/**
	 * @return
	 */
	private ArrayList<Integer> computeCompositeIndices() {
		ArrayList<Integer> indicesToRemove = new ArrayList<Integer>();
		// purge the unique exceptions 0 and 1
		indicesToRemove.add(0);
		indicesToRemove.add(1);

		// implement sieve to remove any multiples of primes up to the upperbound
		// start with:
		// 2
		// 3
		// 5
		// 7
		// 11
		// ... basically, work your way up the list sequentially and execute sieve using
		// current element
		// iff the index of the current element is not already a member of the
		// indicesToRemove ArrayList.
		for (int i = 0; i < storage.size(); i++) {
			if (!indicesToRemove.contains(i)) {
				int d = storage.get(i);
				for (int j = i + d; j < storage.size(); j += d) {
					if (!indicesToRemove.contains(j)) {
						indicesToRemove.add(j);
//						System.out.println(j);
					}
				}
			}
		}
		return indicesToRemove;
	}

	private void purgeElements(ArrayList<Integer> indices) {
		System.out.println("--- Purging composites ---");
		ArrayList<Integer> temp = new ArrayList<Integer>();
		// temp = this.storage;
		// Collections.copy(temp, storage);

		temp = (ArrayList<Integer>) this.storage.clone();

		// System.out.println(temp.size()+" "+storage.size());
		storage.clear();
		// System.out.println(temp.size()+" "+storage.size());
		// for (int i = 0; i < indices.length; i++) {
		for (int i = 0; i < temp.size(); i++) {
			if (!indices.contains(i))
				storage.add(i);
		}
		System.out.println(indices.size() + " elements removed from storage!");
		System.out.println("Storage now has " + storage.size() + " elements.");

	}

	// ref: https://www.programiz.com/java-programming/examples/prime-number
	// Note: Deprecated in lieu of "isPrime()" method.
	// -J.Weaver, 20200803
	public static void primeChecker(int num) {
		boolean flag = false;
		for (int i = 2; i <= num / 2; i++) {
			// condition for nonprime number
			if (num % i == 0) {
				flag = true;
				break;
			}
		}
		if (!flag)
			System.out.println(num + " is a prime number.");
		else
			System.out.println(num + " is not a prime number.");
//		return false;
	}

	// ref: https://www.programiz.com/java-programming/examples/prime-number
	public static boolean IsPrime(int num) {
		boolean flag = false;
		for (int i = 2; i <= num / 2; i++) {
			// condition for nonprime number
			if (num % i == 0) {
				flag = true;
				break;
			}
		}
		if (!flag)
			// System.out.println(num + " is a prime number.");
			return true;
		else
			// System.out.println(num + " is not a prime number.");
			return false;
//		return false;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int max = (int) 2e5;

		long startTime;
//		startTime = System.nanoTime();
//		PrimesList a = new PrimesList(max);
//		System.out.println("Construction time: " + (System.nanoTime() - startTime) * Math.pow(10, -9) + " seconds");
//		a.printSize();
		// a.printStorage();

		startTime = System.nanoTime();
		PrimesList b = new PrimesList(max, true);
		System.out.println("Construction time: " + (System.nanoTime() - startTime) * Math.pow(10, -9) + " seconds");
		b.printSize();
		b.printStorage();
		b.printSum();
		b.sumStorage();
		b.printSum();
		
//		int min = (int) 5;
//		startTime = System.nanoTime();
//		PrimesList c = new PrimesList(min, max);
//		System.out.println("Construction time: " + (System.nanoTime() - startTime) * Math.pow(10, -9) + " seconds");
//		c.printSize();
//		 c.printStorage();
		

//		// test your basic getter and printer methods
//		a.printSize();
//		int size = a.getSize();
//		System.out.println("Does the printer-method and getter-method give same result? "+size);
//		System.out.println();
//		
//		
//		
////		a.printStorage();
//		
//		// some built-in java method here to test here if all remaining elements are prime? or a custom routine?
//		
//		a.printSum();
//		a.sumStorage();
//		a.printSum();
//
//		
//		// test a prime, (v+) randomly
//		int n1 = 16903;
//		primeChecker(n1);
//		System.out.println("is prime? "+IsPrime(n1));
//		int n2 = 16904;
//		primeChecker(n2);
//		System.out.println("is prime? "+IsPrime(n2));

	}

}

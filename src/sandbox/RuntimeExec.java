package sandbox;

//Ref: The following code is copied from 

//   <https://www.viralpatel.net/how-to-execute-command-prompt-command-view-output-java/>.

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class RuntimeExec {
	public StreamWrapper getStreamWrapper(InputStream is, String type) {
		return new StreamWrapper(is, type);
	}

	private class StreamWrapper extends Thread {
		InputStream is = null;
		String type = null;
		String message = null;

		public String getMessage() {
			return message;
		}

		StreamWrapper(InputStream is, String type) {
			this.is = is;
			this.type = type;
		}

		public void run() {
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				StringBuffer buffer = new StringBuffer();
				String line = null;
				while ((line = br.readLine()) != null) {
					buffer.append(line);// .append("\n");
				}
				message = buffer.toString();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}

// this is where the action is
	public static void main(String[] args) {

		if (args.length == 0) {
			Runtime rt = Runtime.getRuntime();
			RuntimeExec rte = new RuntimeExec();
			StreamWrapper error, output;

			try {
				Process proc = rt.exec("ping localhost");
				error = rte.getStreamWrapper(proc.getErrorStream(), "ERROR");
				output = rte.getStreamWrapper(proc.getInputStream(), "OUTPUT");
				int exitVal = 0;

				error.start();
				output.start();
				error.join(3000);
				output.join(3000);
				exitVal = proc.waitFor();
				System.out.println("Output: " + output.message + "\n\nError: " + error.message);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}			
			System.out.println("Done with default hard-coded cmd in main method.");
			System.out.println();
		} else {
			for (String arg : args) {
				System.out.println("arg[i] = " + arg);
				System.out.println();
				// }

				Runtime rt = Runtime.getRuntime();
				RuntimeExec rte = new RuntimeExec();
				StreamWrapper error, output;

				try {
					Process proc = rt.exec(arg);
					error = rte.getStreamWrapper(proc.getErrorStream(), "ERROR");
					output = rte.getStreamWrapper(proc.getInputStream(), "OUTPUT");
					int exitVal = 0;

					error.start();
					output.start();
					error.join(3000);
					output.join(3000);
					exitVal = proc.waitFor();
					System.out.println("Output: " + output.message + "\n\nError: " + error.message);
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("Done with cmd: '"+arg+"'");
				System.out.println();
			}
		}
	}
}

package sandbox;

/**
 * Refs:
 * -<https://www.sqlitetutorial.net/sqlite-java/sqlite-jdbc-driver/>
 * -<https://www.tutorialspoint.com/sqlite/sqlite_java.htm>
 * 
 */

//import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Poseidon
 *
 */
public class Connect {

	/**
	 * 
	 */
	public Connect() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Method: Date initialized: Sep 1, 2020 Return type: void
	 * 
	 * @param args
	 */
	public static void connect() {
		// TODO Auto-generated method stub
		Connection conn = null;
		Statement stmt = null;

		// Establish a name for the sqlite3 database file.
		Date date = new Date(); // This object contains the current date value
		SimpleDateFormat formatter = new SimpleDateFormat("yyyymmddHHmmss");
		String timestamp = formatter.format(date);
		String dbname = "test" + timestamp + ".db";

		try {
			Class.forName("org.sqlite.JDBC");

			String url = "jdbc:sqlite:./dat/" + dbname;
			conn = DriverManager.getConnection(url);
			conn.setAutoCommit(false);
			System.out.println("Connection to SQLite has been established.");

			// Perform actions on the active database.
			stmt = conn.createStatement();
			String sql = "CREATE TABLE COMPANY " + "(ID INT PRIMARY KEY   NOT NULL, "
					+ " NAME           TEXT  NOT NULL, " + " AGE            INT   NOT NULL, "
					+ " ADDRESS        CHAR(50), " + " SALARY         REAL)";
			stmt.executeUpdate(sql);

			sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
					+ " VALUES (1,'Paul', 32, 'California', 20000.00 );";
			stmt.executeUpdate(sql);

			sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " + " VALUES (2, 'Allen', 25, 'Texas', 15000.00 );";
			stmt.executeUpdate(sql);

			sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " + " VALUES (3, 'Teddy', 23, 'Norway', 2000.00 );";
			stmt.executeUpdate(sql);

			sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
					+ " VALUES (4, 'Mark', 25, 'Rich-Mond', 65000.00 );";
			stmt.executeUpdate(sql);

			stmt.close();
			conn.commit();
			conn.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			// System.err.println(e.getMessage());
			System.exit(0);
		} finally {
			// System.out.println("executing 'finally' code block...");
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
		}
		System.out.println("Table and records created and successfully on " + dbname + ".");

	}

	/**
	 * Method: Date initialized: Sep 1, 2020 Return type: void
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		connect();
	}

}


package sandbox;

// Ref: The following code is copied from 
//      <https://ide.tutorialhorizon.com/?gistUrl=https%3A%2F%2Fgist.githubusercontent.com%2Fthmain%2Fbdcf0a00d302c65ba5ba91ea7652f3b1%2Fraw>.

import java.util.LinkedList;

public class CountPaths {

	static int paths = 0;

	static class Graph {
		int vertices;
		LinkedList<Integer>[] adjList;

		Graph(int vertices) {
			this.vertices = vertices;
			adjList = new LinkedList[vertices];
			for (int i = 0; i < vertices; i++) {
				adjList[i] = new LinkedList<>();
			}
		}

		public void addEdge(int source, int destination) {
			adjList[source].addFirst(destination);
		}

		public void countPaths(int source, int destination) {
			count(source, destination);
			System.out.println(
					"No of paths between source: " + source + " and destination: " + destination + " are: " + paths);
		}

		public void count(int start, int destination) {
			//System.out.println(start + ", " + destination);
			if (start == destination) {
				paths++;
			} else {
				for (int i = 0; i < adjList[start].size(); i++) {
					// System.out.println(i+", "+adjList[start].get(i));
					int ajdacentVertex = adjList[start].get(i);
					count(ajdacentVertex, destination);
				}
			}
		}
	}

	public static void main(String[] args) {
//		// Usage example from reference listed at top of class file.
//		int vertices = 6;
//		Graph graph = new Graph(vertices);
//		graph.addEdge(0, 1);
//		graph.addEdge(0, 2);
//		graph.addEdge(1, 2);
//		graph.addEdge(1, 3);
//		graph.addEdge(3, 4);
//		graph.addEdge(2, 3);
//		graph.addEdge(4, 5);
//		int source = 0;
//		int destination = 5;
//		graph.countPaths(source, destination);

		// Custom usage example for PE015, JWeaver, 20200808
		int n_squares = 2;
		int nodes = (int) Math.pow((n_squares + 1), 2);
		System.out.println(nodes);
		Graph g = new Graph(nodes);
		g.addEdge(0,1);
		g.addEdge(0,2);
		g.addEdge(1, 3);
		g.addEdge(1, 4);
		g.addEdge(2, 4);
		g.addEdge(2, 5);
		g.addEdge(3, 6);
		g.addEdge(4, 6);
		g.addEdge(4, 7);
		g.addEdge(5, 7);
		g.addEdge(6, 8);
		g.addEdge(7, 8);
		int src = 0;
		int dest = 8;
		g.countPaths(src, dest);

//		// Custom usage example for PE015, JWeaver, 20200808
//		int n_squares = 2;
//		int nodes = (int) Math.pow((n_squares + 1), 2);
//		System.out.println(nodes);
//		Graph g = new Graph(nodes);
//		
//		for (int i = 0; i<n_squares+1; i++) {
//			g.addEdge
//		}
//		
//		g.addEdge(0, 1);
//		g.addEdge(0, 2);
//		g.addEdge(1, 3);
//		g.addEdge(1, 4);
//		g.addEdge(2, 4);
//		g.addEdge(2, 5);
//		g.addEdge(3, 6);
//		g.addEdge(4, 6);
//		g.addEdge(4, 7);
//		g.addEdge(5, 7);
//		g.addEdge(6, 8);
//		g.addEdge(7, 8);
//		int src = 0;
//		int dest = 8;
//		g.countPaths(src, dest);

	}
}

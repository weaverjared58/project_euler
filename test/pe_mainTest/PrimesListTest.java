/**
 * 
 */
package pe_mainTest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import pe_main.PrimesList;
//import pe_main.PrimesList.*;

/**
 * @author Poseidon
 *
 */
class PrimesListTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link pe_main.PrimesList#sumStorage()}.
	 */
	@Test
	final void testSumStorage() {
		//fail("Not yet implemented"); // TODO
		
		int max = (int)2e3;
		PrimesList a = new PrimesList(max);
		
		
		
		
		//a.printStorage();
		
		// some built-in java method here to test here if all remaining elements are prime? or a custom routine?
		
		a.printSum();
		a.sumStorage();
		a.printSum();
		int sum = a.getStorageSum();
		assertEquals(277050,sum);
	}

	/**
	 * Test method for {@link pe_main.PrimesList#getSize()}.
	 */
	@Test
	final void testGetSize() {
		//fail("Not yet implemented"); // TODO
		int max = (int)2e3;
		PrimesList a = new PrimesList(max);
		
		// test your basic getter and printer methods
		//a.printSize();
		int size = a.getSize();
		assertEquals(303,size);
	}

	/**
	 * Test method for {@link pe_main.PrimesList#sieveComposites()}.
	 */
	@Test
	final void testSieveComposites() {
		//fail("Not yet implemented"); // TODO
		int max = (int)2e3;
		PrimesList a = new PrimesList(max);
		
		// test your basic getter and printer methods
		//a.printSize();
		int size = a.getSize();
		assertEquals(303,size);
		//System.out.println("Does the printer-method and getter-method give same result? "+size);
//		System.out.println();
//		
//		
//		
//		a.printStorage();
//		
//		// some built-in java method here to test here if all remaining elements are prime? or a custom routine?
//		
//		a.printSum();
//		a.sumStorage();
//		a.printSum();
	}

//	/**
//	 * Test method for {@link pe_main.PrimesList#primeChecker(int)}.
//	 */
//	@Test
//	final void testPrimeChecker() {
//		//fail("Not yet implemented"); // TODO
//
//		// test a prime, (v+) randomly
//		int n1 = 16903;
////		PrimesList.primeChecker(n1);
////		System.out.println("is prime? "+PrimesList.isPrime(n1));
//		assert(PrimesList.IsPrime(n1));
//		int n2 = 16904;
////		PrimesList.primeChecker(n2);
////		System.out.println("is prime? "+PrimesList.isPrime(n2));
//		assert(!PrimesList.IsPrime(n2));
//	}
	

	/**
	 * Test method for {@link pe_main.PrimesList#primeChecker(int)}.
	 */
	@Test
	final void testIsPrime() {
		//fail("Not yet implemented"); // TODO

		// test a prime, (v+) randomly
		int n1 = 16903;
//		PrimesList.primeChecker(n1);
//		System.out.println("is prime? "+PrimesList.isPrime(n1));
		assert(PrimesList.IsPrime(n1));
		int n2 = 16904;
//		PrimesList.primeChecker(n2);
//		System.out.println("is prime? "+PrimesList.isPrime(n2));
		assert(!PrimesList.IsPrime(n2));
	}

}

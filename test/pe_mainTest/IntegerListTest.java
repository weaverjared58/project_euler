/**
 * 
 */
package pe_mainTest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import pe_main.IntegerList;

/**
 * @author Poseidon
 *
 */
class IntegerListTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link pe_main.IntegerList#sumStorage()}.
	 */
	@Test
	final void testSumStorage() {
		//fail("Not yet implemented"); // TODO
		int max = (int)2e2;
		IntegerList a = new IntegerList(max);

		assertEquals(19900, a.sumStorage());
		
		a.sieveComposites();
		assertEquals(4227, a.sumStorage());
	}

	/**
	 * Test method for {@link pe_main.IntegerList#SumOfSquares()}.
	 */
	@Test
	final void testSumOfSquares() {
		//fail("Not yet implemented"); // TODO
		int max = 11;
		IntegerList b = new IntegerList(max);
		assertEquals(385, b.SumOfSquares());
	}

	/**
	 * Test method for {@link pe_main.IntegerList#SquareOfSum()}.
	 */
	@Test
	final void testSquareOfSum() {
		//fail("Not yet implemented"); // TODO
		int max = 11;
		IntegerList b = new IntegerList(max);
		assertEquals(3025, b.SquareOfSum());
	}

}

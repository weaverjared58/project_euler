# project_euler

This project is a java/eclipse/oop skill-building exercise aimed at constructing oop-minded solutions to the Project Euler problem set (here <https://projecteuler.net/>) for thoughtful numerical problem solving practice in a fresh programming language, ide, and paradigm.

## Current Progress in this repo:
- PE001 - Complete.
- PE002 - Complete.
- PE006 - Complete.
- PE020 - Complete.
- PE022 - Complete.
- PE025 - Next target.

## Dependencies
The libraries archived in the project "lib" directory were copied from:

commons-math3-3.6.1.jar
- <https://commons.apache.org/proper/commons-math/download_math.cgi>
- The specific download holding this java archive (.jar) was: "commons-math3-3.6.1-bin.zip".

commons-cli-1.4.jar
- <https://commons.apache.org/proper/commons-cli/download_cli.cgi>
- The specific download holding this java archive (.jar) was: "commons-cli-1.4-bin.zip".

sqlite-jdbc-3.32.3.1.jar
- <https://mvnrepository.com/artifact/org.xerial/sqlite-jdbc>
- Jar downloaded directly from site.


## Skill/Knowledge features:
- Designing thoughtful classes for assistance with mathematical problems designed to require computing to solve.
- Modern relational database management using SQLite with JDBC (Implemented for Name score database).
